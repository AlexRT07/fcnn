/*
 * This program implements a Neural Network in order to classificate images that could or not
 * contain stairs.
 * Images are in the form:
 * - Grayscale 2x2 pixels.

 * Compile with C++14 standard
 * -fconcepts-ts linker needed
 * Compile with -D_DEBUG_ to use and debug this code. Don't use in final release

 * Coded by: Alex A. Turriza Suarez, July 2022
*/
#include<iostream>
#include<fstream>
#include<string>
#include<sstream>
#include<vector>

#define CHARS 4 //This is the number of inputs.

using namespace std;

void skipBOM(fstream &f);

/*Some debug functions*/
#ifdef _DEBUG_
void printVector(vector<auto> &v);
#endif

int main(int argc, char ** argv)
{
	/*Verify input format*/
	if(argc != 2)
	{
		cerr << "Uso: ./exe data.csv" << endl;
		return -1;
	}
	/*Declaring variables*/
	string file(argv[1]), line, word;
	vector<int> X;
	vector<double> Y; 
	int cont = 0;
	vector<string> row;
	fstream dataFile;
	dataFile.exceptions(fstream::failbit | fstream::badbit | fstream::eofbit);
	/*Lets open the file and build our data matrix*/
	try
	{
		dataFile.open(file.c_str(), fstream::in);
		skipBOM(dataFile);
		while(getline(dataFile, line))
		{
			row.clear();
			cont = 0;
			stringstream s(line);
			while(getline(s, word, ','))
			{
				if(cont++ < 4)
				{
					X.push_back(stoi(word));
				}
				else
				{
					Y.push_back((double)stoi(word));
				}
			}
		}
	}
	catch(fstream::failure const& e)
	{
		if(dataFile.eof())
		{
			dataFile.close();
			#ifdef _DEBUG_
			printVector(X);
			printVector(Y);
			#endif
		}
		else
		{
			dataFile.close();
			cerr << "Error con archivo de base de datos: " << e.what() << endl;
			return -2;
		}
	}
	return 0;
}

void skipBOM(fstream &f)
{
	/*BOM, Byte Order Mark, is a sequential 3-byte data that could be stored at
	 *the beginning of many files and not avoiding them could cause 
    *a program to suffer a failure*/
	char arr[3];
	f.read(arr, 3);
	if( (unsigned char)arr[0] == 0xEF && 
       (unsigned char)arr[1] == 0xBB && 
       (unsigned char)arr[2] == 0xBF )
	{
		return; /*BOM detected and skipped*/
	}
	f.seekg(0); /*No BOM detected, returning to the beginning of the file*/
}

#ifdef _DEBUG_
void printVector(vector<auto> &v)
{
	cout << "Imprimiendo vector de dimension : " << v.size() << endl;
	for(unsigned int i = 0; i < v.size(); i++)
	{
		cout << v[i] << endl;
	}
	cout << endl;
}
#endif
